# Copyright (c) 2022 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

@arch
arm

@returnValue
KILL_PROCESS

@headFiles
"time.h"
"sys/ioctl.h"
"linux/futex.h"
"sys/resource.h"
"sys/prctl.h"
"sys/mman.h"
"sched.h"
"fcntl.h"
"sys/random.h"
"sys/types.h"
"sys/socket.h"

@priority
futex

@allowList
fdatasync
fsync
ftruncate
ftruncate64
setrlimit
ugetrlimit
mremap
pwrite64
sched_get_priority_max
sched_get_priority_min
getpriority
setpriority
sysinfo
times
uname
get_robust_list
set_robust_list
sched_getaffinity
sigaltstack
futex_time64
brk
mlock
munlock
munmap
mmap2
sched_yield
pause
nanosleep
epoll_create
epoll_wait
epoll_pwait
epoll_create1
epoll_ctl
eventfd
eventfd2
fstat
fstat64
lseek
_llseek
poll
ppoll
pselect6
read
readv
pread64
recvfrom
recvmsg
_newselect
send
sendmsg
sendto
write
writev
pipe
pipe2
gettimeofday
exit
exit_group
wait4
waitid
rt_sigaction
rt_sigprocmask
rt_sigreturn
rt_sigtimedwait
capget
getegid
getegid32
geteuid
geteuid32
getgid
getgid32
getgroups
getgroups32
getpid
getppid
getresgid
getresgid32
getsid
gettid
getuid
getuid32
getresuid
getresuid32
restart_syscall
close
dup
dup2
dup3
shutdown
mincore
memfd_create
faccessat
access
prctl
fcntl
fcntl64
clone
setsockopt

setgroups
setgroups32
setresgid
setresgid32
setresuid
setresuid32
capset
openat
open
socket
connect
readlinkat
readlink
unlink
unlinkat
ioctl
mprotect
mkdirat
set_tid_address
getdents64
madvise
getrandom
clock_gettime64
statx
prlimit64
cacheflush
set_tls
sched_setscheduler
mkdir

@allowListWithArgs
getrusage:if arg0 == RUSAGE_SELF || arg0 == RUSAGE_THREAD; return ALLOW; else return KILL_PROCESS;
clock_getres:if arg0 >= CLOCK_REALTIME && arg0 <= CLOCK_BOOTTIME; return ALLOW; else return KILL_PROCESS;
clock_gettime:if arg0 >= CLOCK_REALTIME && arg0 <= CLOCK_BOOTTIME; return ALLOW; else return KILL_PROCESS;
clock_nanosleep:if arg0 >= CLOCK_REALTIME && arg0 <= CLOCK_BOOTTIME; return ALLOW; else return KILL_PROCESS;
socketpair:if arg0 == AF_UNIX; return ALLOW; else return KILL_PROCESS;
getsockopt:if arg1 == SOL_SOCKET || arg2 == SO_PEEK_OFF; return ALLOW; else return KILL_PROCESS;
